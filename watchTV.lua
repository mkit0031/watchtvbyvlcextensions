
channel_name_to_number = {}
channel_name_to_number["MBS毎日放送"] = 16
channel_name_to_number["ABCテレビ"]   = 15
channel_name_to_number["NHK総合"]     = 24
channel_name_to_number["NHK教育"]     = 13
channel_name_to_number["テレビ大阪"]  = 18
channel_name_to_number["関西テレビ"]  = 17
channel_name_to_number["読売テレビ"]  = 14
channel_name_to_number["サンテレビ"]  = 26
channel_name_to_number["KBS京都"]  = 23
channel_name_to_number["BS朝日"] = 151
channel_name_to_number["BS-TBS"] = 161
channel_name_to_number["BSジャパン"] = 171
channel_name_to_number["BS11"] = 211
channel_name_to_number["BS_TwellV"] = 222
channel_name_to_number["BS日テレ"] = 141
channel_name_to_number["BSフジ"] = 181
channel_name_to_number["BS_NHKBS1"] = 101
channel_name_to_number["BS_NHKBSプレミアム"] = 103


pid = nil
dlg = nil
dropdown = nil
quit_key = 113 -- Qキー

function descriptor()
  return { title = "地デジ視聴" ,capabilities={} }
end

function activate()
	 pid = nil
	 dlg = nil
	 dropdown = nil

 	 local s = vlc.stream( "http://192.168.1.2/epgrec/startrecpt1.php?start=true" )
 	 pid = tonumber(s:read(10))

	 local path = "http://192.168.1.2:8080/15"
	 local name = "ABCテレビ"
	 local mytable = {path = path; name = name}
	 vlc.playlist.add({mytable})
   for name, number in pairs(channel_name_to_number) do
			local path = "http://192.168.1.2:8080/" .. number
			local mytable = {path = path; name = name}
      vlc.playlist.enqueue({mytable})
   end
	 create_dialog()
   vlc.var.add_callback(vlc.object.libvlc() , "key-pressed", key_press)
end

function deactivate()
	 local s = vlc.stream( "http://192.168.1.2/epgrec/stoprecpt1.php?pid=" .. pid )
	 vlc.playlist.clear()
	 vlc.var.del_callback(vlc.object.libvlc() , "key-pressed", key_press)
	 vlc.deactivate()
end

function close()
	 local s = vlc.stream( "http://192.168.1.2/epgrec/stoprecpt1.php?pid=" .. pid )
	 vlc.var.del_callback(vlc.object.libvlc() , "key-pressed", key_press)
	 vlc.deactivate()
end

function create_dialog()
	 dlg = vlc.dialog("地デジ視聴")

	 dropdown = dlg:add_dropdown(1,1,1,1)
   for name, number in pairs(channel_name_to_number) do
      dropdown:add_value(name, number)
   end

   dlg:add_button("視聴", click_select_button, 2,1,1,1)
end

function key_press(var , old , new , data)
	 if new == quit_key then
			dlg:hide()
			dlg:update()
			dlg:delete()
			vlc.deactivate()
	 end
end

function click_select_button()
   local channel = dropdown:get_value()
	 local path = "http://192.168.1.2:8080/" .. channel
	 local name = "テレビ"
	 local mytable = {path = path; name = name}
	 vlc.playlist.add({mytable})
end
