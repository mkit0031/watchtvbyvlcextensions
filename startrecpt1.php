<?php

$start = false;
if( isset( $_GET['start'] ) ){
    $start = $_GET['start'];
}

// recpt1が既に実行されているか確認。
// 実行されていればpidを、されていなければfalseを返す。
function checkPidOfRecpt1(){
    $ret = false;
    $command = "ps aux | grep recpt | awk '$11 ~ /^recpt1/ && /http/{print $2}'";
    $handle = popen($command , "r");
    if($handle){
        $ret = fread($handle , 10);
    }
    pclose($handle);
    return $ret;
}

// recpt1実行。pidを出力する。
function startRecpt1(){
    $handle = popen("recpt1 --b25 --strip --sid hd --http 8080 2>&1" , "r");
    if($handle){
        while(!feof($handle)){
            $buffer = fgets($handle);
            if(preg_match('/pid\s*=\s*(\d+)/', $buffer, $m)){
                print($m[1]."\n");
                break;
            }
        }
    }
    pclose($handle);
}

if( $start == "true" ){
    if( $pid = checkPidOfRecpt1() ){
        print($pid);
    }else{
        startRecpt1();
    }
}
?>