<?php

$pid = false;
if( isset( $_GET['pid'] ) ){
    $pid = intval($_GET['pid']);
}

// 届いたpidがrecpt1のpidか確認する
// pidが数字であることを確認してからこの関数を呼ぶこと
function isPidOfRecpt1($pid){
    $ret = false;
    $command = "ps aux | grep recpt | awk '$1 == \"www-data\" && $11 ~ /^recpt1/ && $2 == " . $pid . " && /http/{print $2}'";
    $handle = popen($command , "r");
    if($handle){
        $buffer = fread($handle , 10);
        if($buffer){
            $ret = true;
        }
    }
    pclose($handle);
    return $ret;
}

// 受け取ったpidを殺す
// pidが存在するか確認してからこの関数を呼ぶこと
function killPid($pid){
    $command = "kill -9 ". $pid . " 2>&1";
    $handle = popen($command , "r");
    if($handle){
        $buffer = fread($handle , 100);
        print($buffer . "<br>");        // デバッグ用
    }
    pclose($handle);
}

if( $pid ){
    if( isPidOfRecpt1($pid) ){
        killPid($pid);
        print($pid);                    // デバッグ用
    }
}

?>